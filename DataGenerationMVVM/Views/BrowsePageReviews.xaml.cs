﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DataGenerationMVVM.Views
{
    /// <summary>
    /// Interaction logic for BrowsePageReviews.xaml
    /// </summary>
    public partial class BrowsePageReviews : Page
    {
        public BrowsePageReviews()
        {
            InitializeComponent();
        }
        public void Users_Browse(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new BrowsePageUsers());
        }

        public void Movies_Browse(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new BrowsePageMovies());
        }
    }
}
