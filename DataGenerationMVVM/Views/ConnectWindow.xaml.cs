﻿using DataGenerationMVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DataGenerationMVVM.Views
{
    /// <summary>
    /// Interaction logic for ConnectWindow.xaml
    /// </summary>
    public partial class ConnectWindow : Window
    {
        public ConnectWindow()
        {
            InitializeComponent();
            DataContext = new ConnectionViewModel();
        }
        public void Connect_Button(object sender, RoutedEventArgs e)
        {
            BrowseWindow browse = new BrowseWindow();
            browse.Show();
            this.Close();
        }
    }
}
