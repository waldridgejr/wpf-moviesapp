﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DataGenerationMVVM.Views
{
    /// <summary>
    /// Interaction logic for BrowsePageUsers.xaml
    /// </summary>
    public partial class BrowsePageUsers : Page
    {
        public BrowsePageUsers()
        {
            InitializeComponent();
        }
        public void Movies_Browse(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new BrowsePageMovies());
        }

        public void Reviews_Browse(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new BrowsePageReviews());
        }
    }
}
