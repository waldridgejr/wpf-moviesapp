﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataGenerationMVVM.ViewModels;

namespace DataGenerationMVVM.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            //load viewModel and set DataContext for our binding paths in the UI
            InitializeComponent();
            DataContext = new DataGeneratorViewModel();
        }

        public void Connect_Button(object sender, RoutedEventArgs e)
        {
            ConnectWindow connect = new ConnectWindow();
            connect.Show();
        }

        public void GenerateAndConnect_Button(object sender, RoutedEventArgs e)
        {
            BrowseWindow browse = new BrowseWindow();
            browse.Show();
        }
    }
}
