﻿using DataGenerationMVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DataGenerationMVVM.Commands
{
    class DataGenerateCommand : ICommand
    {
        //initialize instance of the DataGenerateCommand class
        private DataGeneratorViewModel _viewModel;
        public DataGenerateCommand(DataGeneratorViewModel viewModel)
        {
            _viewModel = viewModel;
        }

        /* We are doing the command from scratch and not inheriting from a 
         * concrete type like routedCommand, etc, so we need to hook our
         * command into the WPF command system
         */
        public event EventHandler CanExecuteChanged
        { 
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        //we can Generate only if the our data fields pass validation
        public bool CanExecute(object parameter)
        {
            return _viewModel.DataGenerator.isValid;
        }

        //tell the viewModel to generate the data
        public void Execute(object parameter)
        {
            _viewModel.GenerateData();
        }
    }
}
