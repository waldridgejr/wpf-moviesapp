﻿using DataGenerationMVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DataGenerationMVVM.Commands
{
    class ConnectCommand : ICommand
    {
        //initialize instance of the DataGenerateCommand class
        private ConnectionViewModel _viewModel;
        public ConnectCommand(ConnectionViewModel viewModel)
        {
            _viewModel = viewModel;
        }

        /* We are doing the command from scratch and not inheriting from a 
         * concrete type like routedCommand, etc, so we need to hook our
         * command into the WPF command system
         */
        public event EventHandler CanExecuteChanged
        { 
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        //we can Connect only if the our data fields pass validation AND the database names are bieing connected to
        public bool CanExecute(object parameter)
        {
            return _viewModel.Connector.isValid;
        }

        //generate the data from the viewModel
        public void Execute(object parameter)
        {
            _viewModel.Connect();
        }
    }
}
