﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataGenerationMVVM.Models;
using System.Diagnostics;
using System.Windows.Input;
using DataGenerationMVVM.Commands;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using RestSharp;

namespace DataGenerationMVVM.ViewModels
{
    class DataGeneratorViewModel
    {
        //instantiate a new DataGenerator model and construct it
        private DataGenerator _dataGenerator;
        public DataGeneratorViewModel()
        {
            _dataGenerator = new DataGenerator();
            GenerateCommand = new DataGenerateCommand(this);
        }

        //so we can access the underlying DataGenerator model
        public DataGenerator DataGenerator
        { 
            get { return _dataGenerator; }
        }

        // gets the GenerateCommand for the viewModel
        public ICommand GenerateCommand
        {
            get;
            private set;
        }

        //do all the data generation and see if we failed or not
        public void GenerateData()
        {
            string error = GenerateMongoData();
        }

        //generate and validate the data, stop if we mess up at any point
        public string GenerateMongoData()
        {
            string error = null;

            if (DataGenerator.databaseClear)
            {
                var client = new MongoClient(DataGenerator.connectionString);
                client.DropDatabase(DataGenerator.movieDatabase);
                client.DropDatabase(DataGenerator.userDatabase);
                client.DropDatabase(DataGenerator.reviewDatabase);
            }

            error = GenerateMovies();
            if (error != null)
                return error;
            error = GenerateUsers();
            if (error != null)
                return error;
            error = GenerateReviews();
            if (error != null)
                return error;

            return error;
        }

        public string GenerateMovies()
        {
            //make a REST client targeting movies, and parse the desired amount of movies to an INT
            var client = new RestClient("https://localhost:5001/api/movies");
            int movie_amount = Convert.ToInt32(DataGenerator.movieAmount);

            //making a random class for release date generation
            Random rnd = new Random();

            //make a new POST request, add the header and body values, then execute
            for (int i = 0; i < movie_amount; i++)
            {
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddJsonBody(new
                {
                    Title = Faker.Company.Name(),
                    ReleaseYear = rnd.Next(1900, 2020),
                    Description = Faker.Lorem.Paragraph(),
                    AverageRating = Math.Round(rnd.NextDouble() * 10, 2)
                });

                IRestResponse response = client.Execute(request);
            }

            string error = ValidateMovies();
            return error;
        }
        public string ValidateMovies()
        {
            string error = null;

            //connect to movies database and movies collection, parse the desired amount of movies to an INT
            var client = new MongoClient(DataGenerator.connectionString);
            IMongoDatabase movies_db = client.GetDatabase(DataGenerator.movieDatabase);
            IMongoCollection<BsonDocument> movies_coll = movies_db.GetCollection<BsonDocument>(DataGenerator.movieCollection);
            int movie_amount = Convert.ToInt32(DataGenerator.movieAmount);

            //check connection
            bool liveConnection = movies_db.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(1000);
            if (!liveConnection)
            {
                error = "error connecting to the movies database";
                return error;
            }

            //count movies and make sure our desired amount was inserted
            var movieCount = movies_coll.CountDocuments(new BsonDocument());
            if (movieCount != movie_amount)
            {
                error = "generated amount of movies did not match desired amount";
                return error;
            }

            return error;
        }

        //generate users, then validate them
        public string GenerateUsers()
        {
            //create a REST client targeting users, and parse the desired amount of users to an INT
            var client = new RestClient("https://localhost:5001/api/users");
            int user_amount = Convert.ToInt32(DataGenerator.userAmount);

            //making a random class for age generation
            Random rnd = new Random();

            //make a new POST request, add the header and body values, then execute
            for (int i = 0; i < user_amount; i++)
            {
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddJsonBody(new
                {
                    FirstName = Faker.Name.First(),
                    LastName = Faker.Name.Last(),
                    Age = rnd.Next(20,30)
                });

                IRestResponse response = client.Execute(request);
            }

            string error = ValidateUsers();
            return error;
        }
        public string ValidateUsers()
        {
            string error = null;

            //connect to user database and user collection, parse the desired amount of users to an INT
            var client = new MongoClient(DataGenerator.connectionString);
            IMongoDatabase users_db = client.GetDatabase(DataGenerator.userDatabase);
            IMongoCollection<BsonDocument> users_coll = users_db.GetCollection<BsonDocument>(DataGenerator.userCollection);
            int user_amount = Convert.ToInt32(DataGenerator.userAmount);

            //check connection
            bool liveConnection = users_db.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(1000);
            if (!liveConnection)
            {
                error = "failed to connect to the users database";
                return error;
            }

            //count movies and make sure our desired amount was inserted
            var userCount = users_coll.CountDocuments(new BsonDocument());
            if (userCount != user_amount)
            {
                error = "generated amount of users did not match desired amount";
                return error;
            }

            return error;
        }

        //generate reviews, then validate them
        public string GenerateReviews()
        {
            //create a REST client targeting reviews, and parse the desired amount of reviews to an INT
            var client = new RestClient("https://localhost:5001/api/reviews");
            int review_amount = Convert.ToInt32(DataGenerator.reviewAmount);

            //create REST clients for users and movies, because we need their info to make a review
            var userClient = new RestClient("https://localhost:5001/api/users");
            var movieClient = new RestClient("https://localhost:5001/api/movies");

            //create a list for the new users, and insert the desired amount into it
            var new_reviews = new List<BsonDocument>();
            for (int i = 0; i < review_amount; i++)
            {
                //this is for randomly accessing the lists of users/movies
                Random rnd = new Random();

                //find a random user and grab their id, first name, and last name, so we can assign a review to them
                var userRequest = new RestRequest(Method.GET);
                userRequest.AddHeader("Content-Type", "application/json");
                IRestResponse<List<User>> userResponse = userClient.Execute<List<User>>(userRequest);
                var userList = userResponse.Data;
                var index = rnd.Next(userList.Count);
                User sample_user = userList[index];
                var id = sample_user.Id;
                var fName = sample_user.FirstName;
                var lName = sample_user.LastName;

                //find a random movie and grab its id and title, so we can assign the review to it
                var movieRequest = new RestRequest(Method.GET);
                movieRequest.AddHeader("Content-Type", "application/json");
                IRestResponse<List<Movie>> movieResponse = movieClient.Execute<List<Movie>>(movieRequest);
                var movieList = movieResponse.Data;
                index = rnd.Next(movieList.Count);
                Movie sample_movie = movieList[index];
                var mId = sample_movie.Id;
                var mTitle = sample_movie.Title;

                //make a new POST request, add the header and body values, then execute
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddJsonBody(new
                {
                    userId = id,
                    UserFirstName = fName,
                    UserLastName = lName,
                    movieId = mId,
                    MovieTitle = mTitle,
                    Body = Faker.Lorem.Paragraph()
                });

                IRestResponse response = client.Execute(request);
            }

            string error = ValidateReviews();
            return error;
        }
        public string ValidateReviews()
        {
            string error = null;

            //connect to review database and review collection, parse the desired amount of reviews to an INT
            var client = new MongoClient(DataGenerator.connectionString);
            IMongoDatabase reviews_db = client.GetDatabase(DataGenerator.reviewDatabase);
            IMongoCollection<BsonDocument> reviews_coll = reviews_db.GetCollection<BsonDocument>(DataGenerator.reviewCollection);
            int review_amount = Convert.ToInt32(DataGenerator.reviewAmount);

            //check connection
            bool liveConnection = reviews_db.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(1000);
            if (!liveConnection)
            {
                error = "failed to connect to the reviews database";
                return error;
            }

            //count movies and make sure our desired amount was inserted
            var reviewCount = reviews_coll.CountDocuments(new BsonDocument());
            if (reviewCount != review_amount)
            {
                error = "generated amount of reviews did not match the desired amount";
                return error;
            }

            return error;
        }

    }
}
