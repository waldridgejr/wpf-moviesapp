﻿using DataGenerationMVVM.Commands;
using DataGenerationMVVM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DataGenerationMVVM.ViewModels
{
    class ConnectionViewModel
    {
        private Connector _connector;
        public ConnectionViewModel()
        {
            _connector = new Connector();
            ConnectCommand = new ConnectCommand(this);
        }

        //so we can access the underlying Connector model
        public Connector Connector
        {
            get { return _connector; }
        }

        // gets the GenerateCommand for the viewModel
        public ICommand ConnectCommand
        {
            get;
            private set;
        }

        //eventually we will do stuff here
        public void Connect()
        {
        }
    }
}
