﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DataGenerationMVVM.Models
{
    public class DataGenerator : INotifyPropertyChanged, IDataErrorInfo
    {
        //constructor
        public DataGenerator()
        {
            connectionString = "mongodb://localhost:27017";
            databaseClear = false;
            movieDatabase = "Movies";
            movieCollection = "MovieDocuments";
            movieAmount = "100";
            userDatabase = "Users";
            userCollection = "UserDocuments";
            userAmount = "25";
            reviewDatabase = "Reviews";
            reviewCollection = "ReviewDocuments";
            reviewAmount = "50";
        }

        //DataGenerator model properties
        private string _connectionString;
        public string connectionString
        {
            get { return _connectionString; }
            set
            {
                if (_connectionString != value)
                {
                    _connectionString = value;
                    onPropertyChanged("connectionString");
                }
            }
        }

        private bool _databaseClear;
        public bool databaseClear
        {
            get { return _databaseClear; }
            set
            {
                if (_databaseClear != value)
                {
                    _databaseClear = value;
                    onPropertyChanged("databaseClear");
                }
            }
        }

        private string _movieDatabase;
        public string movieDatabase
        {
            get { return _movieDatabase; }
            set
            {
                if (_movieDatabase != value)
                {
                    _movieDatabase = value;
                    onPropertyChanged("movieDatabase");
                }
            }
        }

        private string _movieCollection;
        public string movieCollection
        {
            get { return _movieCollection; }
            set
            {
                if (_movieCollection != value)
                {
                    _movieCollection = value;
                    onPropertyChanged("movieCollection");
                }
            }
        }

        private string _movieAmount;
        public string movieAmount
        {
            get { return _movieAmount; }
            set
            {
                if (_movieAmount != value)
                {
                    _movieAmount = value;
                    onPropertyChanged("movieAmount");
                }
            }
        }

        private string _userDatabase;
        public string userDatabase
        {
            get { return _userDatabase; }
            set
            {
                if (_userDatabase != value)
                {
                    _userDatabase = value;
                    onPropertyChanged("userDatabase");
                }
            }
        }

        private string _userCollection;
        public string userCollection
        {
            get { return _userCollection; }
            set
            {
                if (_userCollection != value)
                {
                    _userCollection = value;
                    onPropertyChanged("userCollection");
                }
            }
        }

        private string _userAmount;
        public string userAmount
        {
            get { return _userAmount; }
            set
            {
                if (_userAmount != value)
                {
                    _userAmount = value;
                    onPropertyChanged("userAmount");
                }
            }
        }

        private string _reviewDatabase;
        public string reviewDatabase
        {
            get { return _reviewDatabase; }
            set
            {
                if (_reviewDatabase != value)
                {
                    _reviewDatabase = value;
                    onPropertyChanged("reviewDatabase");
                }
            }
        }

        private string _reviewCollection;
        public string reviewCollection
        {
            get { return _reviewCollection; }
            set
            {
                if (_reviewCollection != value)
                {
                    _reviewCollection = value;
                    onPropertyChanged("reviewCollection");
                }
            }
        }

        private string _reviewAmount;
        public string reviewAmount
        {
            get { return _reviewAmount; }
            set
            {
                if (_reviewAmount != value)
                {
                    _reviewAmount = value;
                    onPropertyChanged("reviewAmount");
                }
            }
        }

        //boolean to track if any of our model properties fail validation
        public bool isValid
        {
            get
            {
                foreach (string property in validatedProperties)
                    if (getValidationError(property) != null)
                        return false;
                return true;
            }
        }

        //INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        protected void onPropertyChanged(string propertyName)

        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        //IDataErrorInfo implementation, needed for displaying our custom errors from XAML
        public string Error
        {
            get { return null; }
        }
        public string this[string propertyName]
        {
            get
            {
                return getValidationError(propertyName);
            }
        }

        //keep track of all the things we're validating
        static readonly string[] validatedProperties =
        {
            "connectionString",
            "movieDatabase",
            "movieCollection",
            "movieAmount",
            "userDatabase",
            "userCollection",
            "userAmount",
            "reviewDatabase",
            "reviewCollection",
            "reviewAmount"
        };

        //check to see if our fields are valid
        string getValidationError(string propertyName)
        {
            string error = null;

            if (propertyName == "connectionString")
                error = ValidateConnectionString();
            else if (propertyName == "movieAmount" || propertyName == "userAmount" || propertyName == "reviewAmount")
                error = ValidateAmounts(propertyName);
            else
                error = ValidateDatabaseCollectionString(propertyName);

            return error;
        }

        //validate connection string and test the connection
        public string ValidateConnectionString()
        {
            if (String.IsNullOrWhiteSpace(connectionString))
                return "Connection string cannot be empty or whitespaces";
            else if (!CanConnect(connectionString))
                return "not a valid mongoDB connection string";

            return null;
        }

        //validate connection string and test the connection
        public string ValidateDatabaseCollectionString(string propertyName)
        {
            if (propertyName == "movieDatabase")
            {
                if (String.IsNullOrWhiteSpace(movieDatabase))
                    return "movie database cannot be empty or whitespaces";
                else
                    return null;
            }
            else if (propertyName == "movieCollection")
            {
                if (String.IsNullOrWhiteSpace(movieCollection))
                    return "movie collection cannot be empty or whitespaces";
                else
                    return null;
            }
            else if (propertyName == "userDatabase")
            {
                if (String.IsNullOrWhiteSpace(userDatabase))
                    return "user database cannot be empty or whitespaces";
                else
                    return null;
            }
            else if (propertyName == "userCollection")
            {
                if (String.IsNullOrWhiteSpace(userCollection))
                    return "user collection cannot be empty or whitespaces";
                else
                    return null;
            }
            else if (propertyName == "reviewDatabase")
            {
                if (String.IsNullOrWhiteSpace(reviewDatabase))
                    return "review database cannot be empty or whitespaces";
                else
                    return null;
            }
            else if (propertyName == "reviewCollection")
            {
                if (String.IsNullOrWhiteSpace(reviewCollection))
                    return "review collection cannot be empty or whitespaces";
                else
                    return null;
            }

            return "property name did not match any database or collection field";
        }

        //validate integer values
        public string ValidateAmounts(string propertyName)
        {
            bool valid;
            int holder;

            if (propertyName == "movieAmount")
            {
                valid = int.TryParse(movieAmount, out holder);
                if (!valid)
                    return "movie amount must be a valid integer";
                return null;
            }
            else if (propertyName == "userAmount")
            {
                valid = int.TryParse(userAmount, out holder);
                if (!valid)
                    return "user amount must be a valid integer";
                return null;
            }
            else if (propertyName == "reviewAmount")
            {
                valid = int.TryParse(reviewAmount, out holder);
                if (!valid)
                    return "review amount must be a valid integer";
                return null;
            }

            return "property name did not match any 'Amount' field";
        }

        //try to make a client with the given connection string and fail if not valid
        public bool CanConnect(string connectionString)
        {
            try
            {
                var client = new MongoClient(connectionString);
                return true;
            }
            catch (MongoDB.Driver.MongoConfigurationException)
            {
                return false;
            }
        }
    }
}
