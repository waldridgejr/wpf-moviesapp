﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DataGenerationMVVM.Models
{
    class Review
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string userId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string movieId { get; set; }

        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string MovieTitle { get; set; }
        public string Body { get; set; }
    }
}
