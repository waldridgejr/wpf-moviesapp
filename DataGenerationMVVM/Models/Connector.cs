﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DataGenerationMVVM.Models
{
    class Connector : INotifyPropertyChanged, IDataErrorInfo
    {
        public Connector()
        {
            connectionString = "mongodb://localhost:27017";
            movieDatabase = "Movies";
            movieCollection = "MovieDocuments";
            userDatabase = "Users";
            userCollection = "UserDocuments";
            reviewDatabase = "Reviews";
            reviewCollection = "ReviewDocuments";
        }

        //INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        protected void onPropertyChanged(string propertyName)

        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        //IDataErrorInfo implementation, needed for displaying our custom errors from XAML
        public string Error
        {
            get { return null; }
        }
        public string this[string propertyName]
        {
            get
            {
                return getValidationError(propertyName);
            }
        }

        private string _connectionString;
        public string connectionString
        {
            get { return _connectionString; }
            set
            {
                if (_connectionString != value)
                {
                    _connectionString = value;
                    onPropertyChanged("connectionString");
                }
            }
        }

        private string _movieDatabase;
        public string movieDatabase
        {
            get { return _movieDatabase; }
            set
            {
                if (_movieDatabase != value)
                {
                    _movieDatabase = value;
                    onPropertyChanged("movieDatabase");
                }
            }
        }

        private string _movieCollection;
        public string movieCollection
        {
            get { return _movieCollection; }
            set
            {
                if (_movieCollection != value)
                {
                    _movieCollection = value;
                    onPropertyChanged("movieCollection");
                }
            }
        }

        private string _userDatabase;
        public string userDatabase
        {
            get { return _userDatabase; }
            set
            {
                if (_userDatabase != value)
                {
                    _userDatabase = value;
                    onPropertyChanged("userDatabase");
                }
            }
        }

        private string _userCollection;
        public string userCollection
        {
            get { return _userCollection; }
            set
            {
                if (_userCollection != value)
                {
                    _userCollection = value;
                    onPropertyChanged("userCollection");
                }
            }
        }

        private string _reviewDatabase;
        public string reviewDatabase
        {
            get { return _reviewDatabase; }
            set
            {
                if (_reviewDatabase != value)
                {
                    _reviewDatabase = value;
                    onPropertyChanged("reviewDatabase");
                }
            }
        }

        private string _reviewCollection;
        public string reviewCollection
        {
            get { return _reviewCollection; }
            set
            {
                if (_reviewCollection != value)
                {
                    _reviewCollection = value;
                    onPropertyChanged("reviewCollection");
                }
            }
        }

        //keep track of all the things we're validating
        static readonly string[] validatedProperties =
        {
            "connectionString",
            "movieDatabase",
            "movieCollection",
            "userDatabase",
            "userCollection",
            "reviewDatabase",
            "reviewCollection",
        };

        //boolean to track if any of our model properties fail validation
        public bool isValid
        {
            get
            {
                foreach (string property in validatedProperties)
                    if (getValidationError(property) != null)
                        return false;
                return true;
            }
        }

        //check to see if our fields are valid
        string getValidationError(string propertyName)
        {
            string error = null;

            if (propertyName == "connectionString")
                error = ValidateConnectionString();
            else if (propertyName == "movieDatabase" || propertyName == "userDatabase" || propertyName == "reviewDatabase")
                error = ValidateDatabases(propertyName);
            else
                error = ValidateCollections(propertyName);

            return error;
        }

        //validate connection string and test the connection
        public string ValidateConnectionString()
        {
            if (String.IsNullOrWhiteSpace(connectionString))
                return "Connection string cannot be empty or whitespaces";
            else if (!CanConnect(connectionString))
                return "not a valid mongoDB connection string";

            return null;
        }

        //try to make a client with the given connection string and fail if not valid
        public bool CanConnect(string connectionString)
        {
            try
            {
                var client = new MongoClient(connectionString);
                return true;
            }
            catch (MongoDB.Driver.MongoConfigurationException e)
            {
                return false;
            }
        }

        //see if our database connections are valid.  Used for the ConnectWindow
        public string ValidateDatabases(string databaseName)
        {
            MongoClient client;

            //try to use the given connection string, fail if we can't create the client
            try
            {
                client = new MongoClient(connectionString);
            }
            catch (MongoDB.Driver.MongoConfigurationException e)
            {
                return "could not connect via connection string";
            }

            if (databaseName == "movieDatabase")
            {
                //check the strings for being empty or white space
                if (String.IsNullOrWhiteSpace(movieDatabase))
                    return "movie database string cannot be empty or whitespaces";

                //try to match the database name to a database on the client
                using (IAsyncCursor<BsonDocument> cursor = client.ListDatabases())
                {
                    while (cursor.MoveNext())
                    {
                        foreach (var doc in cursor.Current)
                        {
                            if (doc["name"] == movieDatabase)
                                return null;
                        }
                    }
                }

                return "could not find database under name: " + movieDatabase;
            }
            else if (databaseName == "userDatabase")
            {
                //check the strings for being empty or white space
                if (String.IsNullOrWhiteSpace(userDatabase))
                    return "movie database string cannot be empty or whitespaces";

                //try to match the database name to a database on the client
                using (IAsyncCursor<BsonDocument> cursor = client.ListDatabases())
                {
                    while (cursor.MoveNext())
                    {
                        foreach (var doc in cursor.Current)
                        {
                            if (doc["name"] == userDatabase)
                                return null;
                        }
                    }
                }

                return "could not find database under name: " + userDatabase;
            }
            if (databaseName == "reviewDatabase")
            {
                //check the strings for being empty or white space
                if (String.IsNullOrWhiteSpace(reviewDatabase))
                    return "movie database string cannot be empty or whitespaces";

                //try to match the database name to a database on the client
                using (IAsyncCursor<BsonDocument> cursor = client.ListDatabases())
                {
                    while (cursor.MoveNext())
                    {
                        foreach (var doc in cursor.Current)
                        {
                            if (doc["name"] == reviewDatabase)
                                return null;
                        }
                    }
                }

                return "could not find database under name: " + userDatabase;
            }

            return "something has gone wrong in database validation";
        }

        public string ValidateCollections(string collectionName)
        {
            MongoClient client;

            //try to use the given connection string, fail if we can't create the client
            try
            {
                client = new MongoClient(connectionString);
            }
            catch (MongoDB.Driver.MongoConfigurationException e)
            {
                return "could not connect via connection string";
            }

            if (collectionName == "movieCollection")
            {
                //check the strings for being empty or white space
                if (String.IsNullOrWhiteSpace(movieCollection))
                    return "movie database string cannot be empty or whitespaces";

                //try to match our collection name to a collection in the database
                var DB = client.GetDatabase(movieDatabase);
                using (IAsyncCursor<BsonDocument> cursor = DB.ListCollections())
                {
                    while (cursor.MoveNext())
                    {
                        foreach (var doc in cursor.Current)
                        {
                            if (doc["name"] == movieCollection)
                                return null;
                        }
                    }
                }

                return "could not find movie collection with name: " + movieCollection;
            }
            else if (collectionName == "userCollection")
            {
                //check the strings for being empty or white space
                if (String.IsNullOrWhiteSpace(userCollection))
                    return "movie database string cannot be empty or whitespaces";

                //try to match our collection name to a collection in the database
                var DB = client.GetDatabase(userDatabase);
                using (IAsyncCursor<BsonDocument> cursor = DB.ListCollections())
                {
                    while (cursor.MoveNext())
                    {
                        foreach (var doc in cursor.Current)
                        {
                            if (doc["name"] == userCollection)
                                return null;
                        }
                    }
                }

                return "could not find user collection with name: " + userCollection;
            }
            if (collectionName == "reviewCollection")
            {
                //check the strings for being empty or white space
                if (String.IsNullOrWhiteSpace(reviewCollection))
                    return "movie database string cannot be empty or whitespaces";

                //try to match our collection name to a collection in the database
                var DB = client.GetDatabase(reviewDatabase);
                using (IAsyncCursor<BsonDocument> cursor = DB.ListCollections())
                {
                    while (cursor.MoveNext())
                    {
                        foreach (var doc in cursor.Current)
                        {
                            if (doc["name"] == reviewCollection)
                                return null;
                        }
                    }
                }

                return "could not find review collection with name: " + reviewCollection;
            }

            return "something has gone wrong in collection ValidateCollections()";
        }
    }
}
